Programul a fost structurat astfel :

Functia main va verifica toate comenziile primite. Prin functia "get_cmd" se genereaza o matrice cu componentele functiei (strtok). In functie de prima parte a oricarei comenzi se va apela functia specifica.

Functia value transforma componenta char in int a comenziilor ce contin si numere. (atoi)

Functia init este corespondentul INITIALIZE , unde am folosit malloc pentur a initializa cu 0 zona de memorie.

Functia dump doar afiseaza memoria conform cerintei.

Functia de alocare , inima programului , va cauta prima zona de memorie in care sa poata fi incadrata zona ceruta de utilizator + zona de gestiune. De asemeni tot aici se modifica si zona de gestiune a elementului anterior si urmator , pentru a-l lega de cel introdus acum (exact ca in cazul listelor de alocare a memoriei dinamic) . In cazul in care nu s-au gasit 'gauri' intre zonele deja alocate , se face o verificare suplimentara la final , in cazul in care s-a ajuns la ultimul element , pentru a calcula zona ramasa libera pana la sfarsitul arenei.

Functia de dealocare va distruge legaturile formate mai sus.
Astfel ca legaturile de tip x -> x+1 -> x+2 devine x -> x+2. Iarasi ca in cazul listelor dinamice.

Functia de FILL functioneaza ca memset

Show free va cauta zonele libere pentru a genera ouputul conform cerintei.La fel si celalalte functii.

Acesta este programul ..



