#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int arena_size;
unsigned char *capu_la_arena;

int get_cmd (char (*parametrii)[32]);
int value (char *s);
void init (int n);
void dump ();
void alocare (int n);
void dealocare (int n);
void umplere (int index , int size , int valoare);
void showfree ();
void showusage ();
void showalloc ();

int get_cmd (char (*parametrii)[32])
{
	char cmd[128] , *token , delimitator[] = " \n";
	int i = 0;

	fgets (cmd , 128 , stdin);

	token = strtok(cmd, delimitator);

  	while( token != NULL )
 	{
  		strcpy (parametrii[i] , token);
  		i++;

  		token = strtok(NULL, delimitator);
   	}

   	return i;
}

int value (char *s)
{
	int x = 0 , i;

	for (i=0 ; s[i] != 0 ; i++)
		x = x*10 + (s[i] - '0');

	return x;
}

void init (int n)
{
	capu_la_arena = (unsigned char*) calloc (n , sizeof(unsigned char));
	arena_size = n;
}

void dump ()
{
	int  i , ji , nr_linii = arena_size / 16;

	for (ji = 0 ; ji <= nr_linii ; ji ++)
	{
		printf("%08X\t", ji*16);
		for (i=0 ; i < 8 && ji*16 + i < arena_size; i++)
			printf("%02X ", *(capu_la_arena + ji*16 + i));

		if (ji*16 + i < arena_size) printf(" ");

		for (i=8 ; i < 16 && ji*16 + i < arena_size; i++)
			printf("%02X ", *(capu_la_arena + ji*16 + i));
		printf("\n");
	}
	if ((ji - 1)*16 != arena_size)
        printf("%08X\n", arena_size);
}

void alocare (int n)
{
    int *checker = (int*)capu_la_arena , *aux , *urm = NULL;
    unsigned char *point = checker + 1;

if (*checker - 4 < n + 12)
{   
    checker = (int*)(capu_la_arena + *checker);
    while (*checker != 0)
    {
        if ( *(checker) - *(checker + 2) - (int)((unsigned char*)checker - capu_la_arena) >= n + 12)
        {
            point = (unsigned char*)checker + *(checker + 2);
            break;
        }
        checker = (int*)(capu_la_arena + *checker);
    }
    if (*checker == 0 && checker != capu_la_arena)
        if (arena_size - (int)((unsigned char*)checker - capu_la_arena) - *(checker + 2) >= n + 12)
        {
            point = (unsigned char*)checker + *(checker + 2);
        }
        else
        {
            printf("%d\n" , 0);
            return ;
        }
}

    aux = point;

    *aux = *checker;

    aux++;
    *aux = (int)((unsigned char*)checker - capu_la_arena);

    aux++;
    *aux = n + 12;

    if (*checker != 0)
    {
        urm = capu_la_arena + *checker;
        *(urm + 1) = (int)(point - capu_la_arena);
    }

    *checker = (int)(point - capu_la_arena);

    printf("%d\n", (int)(point- capu_la_arena + 12));
}

void dealocare (int n)
{
    unsigned char *point = capu_la_arena + n - 12;
    int *checker = (int*)point , *aux;
    
    if (*checker != 0)
    {
    aux = capu_la_arena + *checker + 4;
    *aux = *(checker + 1);
    }

    aux = capu_la_arena + *(checker + 1);
    *aux = *checker;
}

void umplere (int index , int size , int valoare)
{
    unsigned char *point = capu_la_arena + index;
    int ji;

    for (ji = 0 ; ji < size && ji < arena_size; ji++)
        *(point + ji) = valoare;
}

void showfree ()
{
    int *checker = (int*)capu_la_arena , cnt_regions = 0 , cnt_bytes = 0;

    if (*(checker) == 0)
    {
        printf("%d blocks (%d bytes) free\n", 1 , arena_size - 4);
        return ;
    }
    else 
    {
        if (*(checker) - 4 > 0)
        {
            cnt_bytes +=  *(checker) - 4;
            cnt_regions ++;
        }

        checker = capu_la_arena + *checker;
    }

    while (*checker != 0)
    {
        if ( *(checker) - *(checker + 2) - (int)((unsigned char*)checker - capu_la_arena) > 0)
        {
            cnt_bytes +=  *(checker) - *(checker + 2) - (int)((unsigned char*)checker - capu_la_arena);
            cnt_regions ++;
        }

        checker = capu_la_arena + *checker;
    }

    if (arena_size - (int)((unsigned char*)checker - capu_la_arena) - *(checker + 2) > 0)
    {
        cnt_bytes +=  arena_size - (int)((unsigned char*)checker - capu_la_arena) - *(checker + 2);
        cnt_regions ++;
    }

    printf("%d blocks (%d bytes) free\n", cnt_regions , cnt_bytes); 
}

void showusage ()
{
    int *checker = (int*)capu_la_arena , cnt_regions = 0 , cnt_bytes = 0 , effi , frag , cnt_free = 0;


    if (*(checker) != 0)
    {
        if (*(checker) - 4 > 0)
            cnt_free ++;

        cnt_regions ++;
        checker = capu_la_arena + *checker;
    }

    while (*checker != 0)
    {

        if (*(checker) - *(checker + 2) - (int)((unsigned char*)checker - capu_la_arena) > 0)
            cnt_free ++;

        cnt_bytes +=  *(checker + 2) - 12;
        cnt_regions ++;

        checker = capu_la_arena + *checker;
    }
    cnt_bytes +=  *(checker + 2) - 12;

    if (arena_size - (int)((unsigned char*)checker - capu_la_arena) - *(checker + 2) > 0)
        cnt_free ++;

    printf("%d blocks (%d bytes) used\n", cnt_regions , cnt_bytes); 

    effi = (cnt_bytes * 100) / (cnt_bytes + cnt_regions * 12 + 4);
    printf("%d %% efficiency\n", effi);

    if (cnt_regions == 0)
        frag = 0;
    else
        frag = (cnt_free - 1) * 100 / cnt_regions;
    printf("%d %% fragmentation\n", frag);
}

void showalloc ()
{
    int *checker = (int*)capu_la_arena;

    printf("OCCUPIED 4 bytes\n");

    if (*(checker) != 0)
    {
        if (*(checker) - 4 > 0)
            printf("FREE %d bytes\n" , *(checker) - 4);

        checker = capu_la_arena + *checker;
    }
    else
    {
        printf("FREE %d bytes\n" , arena_size - 4);
        return ;
    }

    while (*checker != 0)
    {
        printf("OCCUPIED %d bytes\n" , *(checker + 2));

        if ( *(checker) - *(checker + 2) - (int)((unsigned char*)checker - capu_la_arena) > 0)
            printf("FREE %d bytes\n" , *(checker) - *(checker + 2) - (int)((unsigned char*)checker - capu_la_arena));

        checker = capu_la_arena + *checker;
    }
    printf("OCCUPIED %d bytes\n" , *(checker + 2));

    if (arena_size - (int)((unsigned char*)checker - capu_la_arena) - *(checker + 2) > 0)
        printf("FREE %d bytes\n" , arena_size - (int)((unsigned char*)checker - capu_la_arena) - *(checker + 2));
}

int main ()
{

	char  params [4][32];
	int nr_params , ji;

	do
	{
		nr_params = get_cmd(params);

		if (strcmp (params[0] , "INITIALIZE") == 0)
			init (value (params[1]));

		if (strcmp (params[0] , "FINALIZE") == 0)
			free (capu_la_arena);

		if (strcmp (params[0] , "DUMP") == 0)
			dump ();

		if (strcmp (params[0] , "ALLOC") == 0)
			alocare (value(params[1]));

        if (strcmp (params[0] , "FREE") == 0)
            dealocare (value(params[1]));

        if (strcmp (params[0] , "FILL") == 0)
            umplere (value(params[1]) , value(params[2]) , value(params[3]));

        if (strcmp (params[0] , "SHOW") == 0)
        {
            if (strcmp (params[1] , "FREE") == 0)
                showfree ();

            if (strcmp (params[1] , "USAGE") == 0)
                showusage ();

            if (strcmp (params[1] , "ALLOCATIONS") == 0)
                showalloc ();
        }

	}while (strcmp(params[0] , "FINALIZE"));

	return 0;
}